import React from 'react';
// import { SkillsItem } from './SkillsItem';

import html from '../../assets/icons/html.svg';
import css from '../../assets/icons/css.svg';
import js from '../../assets/icons/js.svg';
import react from '../../assets/icons/react.svg';
import typescript from '../../assets/icons/typescript.svg';
import next from '../../assets/icons/next.svg';
import pug from '../../assets/icons/pug.svg';
import tailwind from '../../assets/icons/tailwind.svg';
import nodejs from '../../assets/icons/nodejs.svg';
import postgresql from '../../assets/icons/postgresql.svg';
import redux from '../../assets/icons/redux.svg';
import mongodb from '../../assets/icons/mongodb.svg';
import sql from '../../assets/icons/sql.svg';
import express from '../../assets/icons/express.svg';
import sass from '../../assets/icons/sass.svg';
import git from '../../assets/icons/git.svg';
import gitlab from '../../assets/icons/gitlab.svg';
import figma from '../../assets/icons/figma.svg';
import blender from '../../assets/icons/blender.svg';

import { SkillCircle } from './SkillsCircle';
import gsap from 'gsap';

export const Skills = () => {
  const techRef = React.useRef();
  const ref = React.useRef();

  const [techName, setTechName] = React.useState('');

  const handleTechEnter = (str) => {
    setTechName(str);
    gsap.to(techRef.current, { opacity: 1, duration: 0.5, delay: 5 });
    // gsap.to(techRef.current, { opacity: 1, duration: 0.2 });
  };

  const handleTechLeave = () => {
    // gsap.to(techRef.current, { opacity: 0, duration: 0.2 });
  };
  return (
    <div className=" relative bg-gray-900 text-white py-16">
      <div className="container">
        <div className=" flex items-center justify-center h-screen ">
          <div className="relative w-80 h-80 border-[25px]  rounded-full flex items-center justify-center">
            <span className="w-3 h-96 bg-gray-900 absolute rotate-[-30deg]"></span>
            <span className="w-3 h-40  bg-gray-900 absolute rotate-[65deg] left-0 top-28"></span>

            <h1 className="absolute opacity-0" ref={techRef}>
              {techName}
            </h1>

            <div className="relative w-full h-full">
              {/* Frontend ========================================= */}
              <div className="absolute -right-[40px] -top-[70px] ">
                <div className="relative z-[1]">
                  <SkillCircle icon={html} width={10} height={10} bg={'#22D7FD'} rotate={'0deg'} />
                </div>
                <div className="absolute -top-[80px] left-[20px] -rotate-[35deg]">
                  <SkillCircle icon={pug} width={10} height={10} rotate={'35deg'} bg={'#22D7FD'} />
                </div>
              </div>
              <div className="absolute -right-[55%] top-[42%] transform -translate-y-1/2 -translate-x-1/2">
                <div className="rotate-45">
                  <SkillCircle icon={css} width={10} height={10} rotate={'-45deg'} bg={'#22D7FD'} />
                </div>
                <div className="absolute -top-[70px] left-[50px] -rotate-12">
                  <SkillCircle icon={tailwind} rotate={'12deg'} bg={'#22D7FD'} />
                </div>
                <div className="absolute -top-[0px] left-[80px] rotate-45">
                  <SkillCircle icon={sass} rotate={'-45deg'} bg={'#22D7FD'} />
                </div>
              </div>
              <div className="absolute -bottom-[20px] -right-[85px] ">
                <div className="absolute left-[80px] bottom-[30px] rotate-[20deg]">
                  <SkillCircle icon={react} rotate={'-20deg'} bg={'#22D7FD'} />
                </div>
                <div className="rotate-[80deg]">
                  <SkillCircle icon={js} rotate={'-80deg'} bg={'#22D7FD'} />
                </div>
                <div className="absolute left-[160px] -top-[30px] rotate-[45deg]">
                  <SkillCircle icon={redux} rotate={'-45deg'} bg={'#22D7FD'} />
                </div>
                <div className="absolute left-[70px] top-[50px] rotate-[80deg]">
                  <SkillCircle icon={typescript} rotate={'-80deg'} bg={'#22D7FD'} />
                </div>
                <div className="absolute left-[0px] top-[85px] rotate-[130deg]">
                  <SkillCircle
                    icon={next}
                    width={10}
                    height={10}
                    rotate={'-130deg'}
                    bg={'#22D7FD'}
                  />
                </div>
              </div>
              {/*Backend ======================================================= */}

              <div className="absolute right-full -top-[70px]">
                <div className="-rotate-[85deg]">
                  <SkillCircle icon={nodejs} bg={'#6CC24A'} rotate={'85deg'} />
                </div>
                <div className="absolute -left-[85px] -top-[5px] -rotate-[135deg]">
                  <SkillCircle icon={express} bg={'#6CC24A'} rotate={'135deg'} />
                </div>
              </div>
              <div className="absolute  -left-[47%] top-[50%] -translate-y-1/2 ">
                <div className="-rotate-[130deg]">
                  <SkillCircle icon={sql} bg={'#6CC24A'} rotate={'130deg'} />
                </div>
                <div className="absolute -left-[120%] -top-[120%] -rotate-[90deg]">
                  <SkillCircle icon={mongodb} bg={'#6CC24A'} rotate={'90deg'} />
                </div>
                <div className="absolute -left-[120%] top-[120%] -rotate-[180deg]">
                  <SkillCircle icon={postgresql} bg={'#6CC24A'} rotate={'180deg'} />
                </div>
              </div>

              {/* Other ==================================================== */}
              <div className="absolute -left-[65px] top-full">
                <div className="-rotate-[180deg]">
                  <SkillCircle icon={git} width={10} height={10} bg={'#EA882B'} rotate={'180deg'} />
                </div>
                <div className="absolute -left-[84px] top-[15px] -rotate-[145deg]">
                  <SkillCircle icon={gitlab} bg={'#EA882B'} rotate={'145deg'} />
                </div>
              </div>
              <div className="absolute left-[20%] -bottom-[110px] transform -translate-x-1/2">
                <div className="rotate-[155deg]">
                  <SkillCircle icon={figma} bg={'#EA882B'} rotate={'-155deg'} />
                </div>
              </div>
              <div className="absolute left-[58%] -bottom-[115px] rotate-[120deg]">
                <div>
                  <SkillCircle icon={blender} bg={'#EA882B'} rotate={'-120deg'} />
                </div>
              </div>
              {/* ==================================================== */}
            </div>

            {/* <div
              className="absolute -left-[290px] -top-[100px] w-[100%] h-[100%] outline -rotate-[30deg] opacity-0 z-[1]"
              onMouseEnter={() => handleTechEnter('Backend')}></div>
            <div
              className="absolute -right-[100%] -top-[220px] w-[150%] h-[220%] outline -rotate-[30deg] opacity-0 z-[2]"
              onMouseEnter={() => handleTechEnter('Frontend')}></div>
            <div
              className="absolute -left-[50%] -bottom-[80%] w-[120%] h-[120%] outline -rotate-[30deg] opacity-0 z-[3]"
              onMouseEnter={() => handleTechEnter('Other skills')}></div> */}
          </div>
        </div>
      </div>
    </div>
  );
};
