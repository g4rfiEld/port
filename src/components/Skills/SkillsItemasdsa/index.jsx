import React from 'react';

import html from '../../../assets/icons/html.svg';
import css from '../../../assets/icons/css.svg';
import js from '../../../assets/icons/js.svg';
import react from '../../../assets/icons/react.svg';
import typescript from '../../../assets/icons/typescript.svg';
import next from '../../../assets/icons/next.svg';
import { SkillCircle } from '../SkillsCircle';

export const SkillsItem = ({ icon, nextSkill, left, bottom, transform }) => {
  return (
    <>
      <SkillCircle icon={icon} left={left} bottom={bottom} transform={transform} />
      {nextSkill && <SkillsItem {...nextSkill} />}
    </>
  );
};

{
  /* <div className="w-[50px] h-[50px] bg-[#5A5FE1] flex items-center justify-center rounded-full before:content-[''] before:absolute before:w-2 before:h-10 before:rotate-[45deg] before:left-[-10%] before:top-[70%] before:bg-[#5A5FE1]"> */
}

// import React from 'react';
// import { SkillsItem } from './SkillsItem';

// const skillsJs = {
//   icon: js,
//   nextSkill: {
//     icon: react,
//     nextSkill: {
//       icon: js,
//       nextSkill: {
//         icon: react,
//         nextSkill: {
//           icon: js,
//         },
//       },
//     },
//   },
// };
// const skillsCss = {
//   icon: typescript,
//   nextSkill: {
//     icon: react,
//     nextSkill: {
//       icon: js,
//       nextSkill: {
//         icon: css,
//         nextSkill: {
//           icon: html,
//         },
//       },
//     },
//   },
// };
// const skillsHtml = {
//   icon: typescript,
//   nextSkill: {
//     icon: react,
//     nextSkill: {
//       icon: js,
//       nextSkill: {
//         icon: css,
//         nextSkill: {
//           icon: html,
//         },
//       },
//     },
//   },
// };

// export const Skills = () => {
//   return (
//     <div className="relative bg-gray-900 text-white">
//       <div className="container">
//         <div className="flex items-center justify-center h-screen">
//           <div className="relative w-[350px] h-[350px] border-[30px] rounded-full flex items-center ">
//             {/* <span className="w-3 h-96 bg-gray-900 absolute rotate-[-30deg] after:content-[''] after:absolute after:w-[320px] after:h-[320px] after:bg-red-600 after:left-[-140px] after:top-[20px] after:rounded-full ove"></span> */}
//             <span className="w-3 h-96 bg-gray-900 absolute leff rotate-[-30deg]"></span>
//             <span className="w-3 h-40  bg-gray-900 absolute rotate-[75deg] left-0 top-28"></span>
//             <div className="absolute bottom-1/2 left-full ">
//               <SkillsItem />
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };
