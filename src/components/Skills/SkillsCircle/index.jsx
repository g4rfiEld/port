import React from 'react';

export const SkillCircle = ({ icon, rotate, bg, width, height }) => {
  return (
    // bg-[#5A5FE1]
    <div>
      <div
        style={{ background: bg }}
        className={`relative w-[50px] h-[50px] bg-[${bg}] flex items-center justify-center rounded-full z-[2]`}>
        <img
          src={icon}
          alt=""
          className={`w-${width ? width : '8'} h-${height ? height : '8'}`}
          style={{ rotate: `${rotate}` }}
        />
      </div>
      <div
        className={`absolute w-1 h-10  rotate-45 -left-2 top-8`}
        style={{ background: bg }}></div>
    </div>
  );
};
