import React from 'react';

import { SkillFrontend } from './SkillFrontend';
import { SkillBackend } from './SkillBackend';
import { SkillOther } from './SkillOther';
import { SkillPopup } from './SkillPopup';
import { useParams } from 'react-router-dom';

export const SkillCircle = () => {
  const { name } = useParams();

  return (
    <div className="container">
      <div className="relative w-full h-[100vh] overflow-hidden ">
        {name === 'frontend' && <SkillFrontend />}
        {name === 'backend' && <SkillBackend />}
        {name === 'other' && <SkillOther />}
      </div>
      <SkillPopup />

      {/* <div className="absolute right-0 w-[40%] h-full bg-gray-900 opacity-90 py-12">
        <div className="px-12 relative before:absolute before:left-0 before:top-0 before:w-[6px] before:bg-white before:h-full">
          <span className="text-xl ">frontend</span>
          <h1 className="uppercase text-6xl font-bold mt-4">Html</h1>
        </div>
        <div className=" mt-12">
          <div className="bg-[#00BFF0] text-black px-12 py-7">
            <span className="text-[22px] font-medium ">Brief information</span>
          </div>
          <p className="px-12 mt-10 leading-7">
            The fundamental language for web development used to create websites. HTML gives the
            browser instructions on how to display information on the page
          </p>
        </div>
      </div> */}
    </div>
  );
};

{
  /* <div className="absolute left-[44%] top-[30%] -rotate-[70deg] ">
              <SkillItem icon={css} rotate={'70deg'} />
            </div>
            <div className="absolute left-[55%] top-[18%] -rotate-[45deg]">
              <SkillItem icon={sass} rotate={'45deg'} />
            </div>
            <div className="absolute left-[70%] top-[15%] -rotate-[10deg] ">
              <SkillItem icon={tailwind} rotate={'10deg'} />
            </div>
            <div className="absolute left-[42%] top-[50%]  -translate-y-[50%] -translate-x-[50%]">
              <SkillItem icon={html} />
            </div>
            <div className="absolute left-[56%] top-[50%]  -translate-y-[50%] -translate-x-[50%] ">
              <SkillItem icon={js} />
            </div>
            <div className="absolute left-[69%] top-[50%]  -translate-y-[50%] -translate-x-[50%]">
              <SkillItem icon={react} />
            </div>
            <div className="absolute left-[83%] top-[50%]  -translate-y-[50%] -translate-x-[50%]">
              <SkillItem icon={typescript} />
            </div>
            <div className="absolute left-[44%] top-[63%] rotate-[70deg] ">
              <SkillItem icon={pug} rotate={'-70deg'} />
            </div> */
}
