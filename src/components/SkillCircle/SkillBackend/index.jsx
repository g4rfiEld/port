import React from 'react';
import { SkillItem } from '../SkillItem';
import nodejs from '../../../assets/icons/nodejs.svg';
import sql from '../../../assets/icons/sql.svg';
import postgresql from '../../../assets/icons/postgresql.svg';
import mongodb from '../../../assets/icons/mongodb.svg';
import express from '../../../assets/icons/express.svg';

export const SkillBackend = () => {
  return (
    <div
      className="absolute  w-[1250px] h-[1250px] border-2 rounded-full left-[50%] top-[15%] -translate-x-[50%] border-[#70747d9b] before:absolute before:inset-[0px] before:border-transparent before:border-l-[#21A366] before:border-2 before:rounded-full before:animate-spin 
    
    max-lg:w-[1100px] max-lg:h-[1100px] max-md:w-[700px] max-md:h-[700px] max-sm:w-[380px] max-sm:h-[380px]">
      <SkillItem bg={'#082532'} icon={mongodb} left={'35%'} top={'-3%'} />
      <div
        className="absolute  w-[950px] h-[950px] border-2 rounded-full left-[50%] top-[50%] -translate-x-[50%] -translate-y-[50%] border-[#70747d9b] before:absolute before:inset-[0px] before:border-transparent before:border-l-[#21A366] before:border-2 before:rounded-full before:animate-spin-some-slow
      max-lg:w-[800px] max-lg:h-[800px] max-md:w-[500px] max-md:h-[500px] max-sm:w-[270px] max-sm:h-[270px]
      ">
        <SkillItem bg={'#ffffff'} icon={postgresql} left={'22%'} top={'0%'} />
        <div
          className="absolute  w-[650px] h-[650px] border-2 rounded-full left-[50%] top-[50%]
        -translate-x-[50%] -translate-y-[50%] border-[#70747d9b] before:absolute before:inset-[0px]
        before:border-transparent before:border-l-[#21A366] before:border-2 before:rounded-full 
        before:animate-spin-slow max-lg:w-[500px] max-lg:h-[500px] max-md:w-[300px] max-md:h-[300px] 
        max-sm:w-[170px] max-sm:h-[170px]">
          <SkillItem bg={'#F15B6C'} icon={sql} left={'16%'} top={'0%'} />
          <SkillItem bg={'#E0C600'} icon={express} left={'65%'} top={'-5%'} />
          <div
            className="absolute  w-[350px] h-[350px] border-2 rounded-full left-[50%] top-[50%] 
          -translate-x-[50%] -translate-y-[50%]  
          border-[#70747d9b] before:absolute before:inset-[0px] before:border-transparent 
          before:border-l-[#21A366] before:border-2 before:rounded-full before:animate-spin-very-slow
          max-lg:w-[250px] max-lg:h-[250px] max-md:w-[150px] max-md:h-[150px] max-sm:w-[70px] max-sm:h-[70px]
          ">
            <SkillItem bg={'#46473D'} icon={nodejs} left={'40%'} top={'-10%'} />
          </div>
        </div>
      </div>
    </div>
  );
};
