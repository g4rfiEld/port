import React from 'react';
import cube from '../../../assets/icons_skill/cube.svg';
export const SkillPopup = () => {
  return (
    <div className="absolute right-0 bottom-0 h-full bg-[#161C28] w-[30%] z-20  opacity-90 py-8 overflow-auto max-lg:w-[40%] max-md:w-[50%] max-[660px]:w-[75%]  max-[500px]:w-full">
      <div className="flex px-5 items-center">
        <div className="w-10 h-10 overflow-hidden max-[660px]:w-8 max-[660px]:h-8">
          <img src={cube} alt="" />
        </div>
        <h1 className="text-3xl font-bold pl-2 max-[660px]:text-2xl max-[500px]:text-xl">
          Blender skill
        </h1>
      </div>
      <div className=" mt-14 max-md:mt-8">
        <div className="px-5">
          <p className="mb-3">Progress</p>
          <div className="relative w-full h-12 bg-black p-3 border-2 border-white">
            <div className="w-20 h-full bg-white"></div>
          </div>
        </div>
        {/* <div className="grow-0 shrink-0 basis-[30%] h-full p-2 rounded-lg">
      <img src={cube} alt="" />
    </div> */}
        <div className="bg-[#7D62FF] py-4 mt-14 max-[660px]:mt-10">
          <span className="px-5 text-xl font-mono whitespace-nowrap max-[500px]:text-[16px]">
            Brief information
          </span>
        </div>
        <p className="text-[16px] leading-6 px-5 mt-5 max-[660px]:text-[14px] max-">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit, numquam voluptas impedit non
          quisquam pariatur dolorum voluptatum ad aut vitae perspiciatis quidem quis voluptatem
          animi? Repellat id vel quos non.
        </p>
        <button className="relative left-[50%] top-0 mt-20 bottom-16 -translate-x-[50%] inline-block h-[50px] leading-[50px] font-mono px-10 border-2 text-white max-[500px]:mt-14">
          some work
        </button>
      </div>
    </div>
  );
};
