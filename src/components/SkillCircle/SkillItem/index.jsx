import React from 'react';

export const SkillItem = ({ bg, icon, left, top }) => {
  return (
    <div
      className="absolute  flex items-center justify-center cursor-pointer"
      style={{ left, top }}>
      <div
        className="relative  w-[80px] h-[80px] rounded-md z-[1] p-3 flex items-center justify-center  overflow-hidden 
        max-lg:w-[70px] max-lg:h-[70px] max-md:w-[45px] max-md:h-[45px] max-sm:w-[35px] max-sm:h-[35px] max-md:p-2"
        style={{ background: bg }}>
        <img src={icon} alt="" className="w-auto h-auto" />
      </div>
      <div
        className="absolute w-[100px] h-[100px] border-[#fff] border-2 max-lg:w-[90px] max-lg:h-[90px] max-md:w-[55px] rounded-md max-md:h-[55px] 
        max-sm:w-[42px] max-sm:h-[42px]
      before:absolute before:left-[13.5%] before:top-[14%] before:max-sm:left-[15.5%] before:max-sm:top-[14.5%] before:w-[70px] before:h-[70px]  
       before:border-2 before:animate-ping-slow before:max-lg:w-[60px] before:rounded-md before:max-lg:h-[60px] before:max-md:w-[40px] before:max-md:h-[40px] before:max-sm:w-[27px] before:max-sm:h-[27px]"></div>

      {/* <div className="fixed right-0 -top-[-100%] bg-[#161C28] w-full h-full z-[1]">
        <div className="header">
          <div>
            <p>Progress</p>
            <p>75%</p>
          </div>
          <div className="progress"></div>
        </div>
      </div> */}
    </div>
  );
};
