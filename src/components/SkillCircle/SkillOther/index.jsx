import React from 'react';

import git from '../../../assets/icons/git.svg';
import github from '../../../assets/icons/github.svg';
import gitlab from '../../../assets/icons/gitlab.svg';
import figma from '../../../assets/icons/figma.svg';
import blender from '../../../assets/icons/blender.svg';
import css from '../../../assets/icons/css.svg';
import js from '../../../assets/icons/js.svg';
import sass from '../../../assets/icons/sass.svg';
import tailwind from '../../../assets/icons/tailwind.svg';
import { SkillItem } from '../SkillItem';

export const SkillOther = () => {
  return (
    <div
      className="absolute  w-[1250px] h-[1250px] border-2 rounded-full left-[50%] top-[15%] -translate-x-[50%] border-[#70747d9b] before:absolute before:inset-[0px] before:border-transparent before:border-l-[#e66060] before:border-2 before:rounded-full before:animate-spin 
  
  max-lg:w-[1100px] max-lg:h-[1100px] max-md:w-[700px] max-md:h-[700px] max-sm:w-[380px] max-sm:h-[380px]">
      <SkillItem bg={'#FF6D00'} icon={blender} left={'50%'} top={'-3%'} />
      {/* <SkillItem bg={'#1976D2'} icon={typescript} left={'45%'} top={'-3%'} /> */}
      <div
        className="absolute  w-[950px] h-[950px] border-2 rounded-full left-[50%] top-[50%] -translate-x-[50%] -translate-y-[50%] border-[#70747d9b] before:absolute before:inset-[0px] before:border-transparent before:border-l-[#e66060] before:border-2 before:rounded-full before:animate-spin-some-slow
    max-lg:w-[800px] max-lg:h-[800px] max-md:w-[500px] max-md:h-[500px] max-sm:w-[270px] max-sm:h-[270px]
    ">
        <SkillItem bg={'#fff'} icon={figma} left={'22%'} top={'0%'} />
        {/* <SkillItem bg={'#720BCD'} icon={react} left={'55%'} top={'-4%'} /> */}
        <div
          className="absolute  w-[650px] h-[650px] border-2 rounded-full left-[50%] top-[50%]
      -translate-x-[50%] -translate-y-[50%] border-[#70747d9b] before:absolute before:inset-[0px]
      before:border-transparent before:border-l-[#e66060] before:border-2 before:rounded-full 
      before:animate-spin-slow max-lg:w-[500px] max-lg:h-[500px] max-md:w-[300px] max-md:h-[300px] 
      max-sm:w-[170px] max-sm:h-[170px]">
          <SkillItem bg={'#2f2e2e'} icon={github} left={'16%'} top={'0%'} />
          <SkillItem bg={'#fff'} icon={gitlab} left={'65%'} top={'-3%'} />
          <div
            className="absolute  w-[350px] h-[350px] border-2 rounded-full left-[50%] top-[50%] 
        -translate-x-[50%] -translate-y-[50%]  
        border-[#70747d9b] before:absolute before:inset-[0px] before:border-transparent 
        before:border-l-[#e66060] before:border-2 before:rounded-full before:animate-spin-very-slow
        max-lg:w-[250px] max-lg:h-[250px] max-md:w-[150px] max-md:h-[150px] max-sm:w-[70px] max-sm:h-[70px]
        ">
            <SkillItem bg={'#F05539'} icon={git} left={'40%'} top={'-10%'} />
          </div>
        </div>
      </div>
    </div>
  );
};
