import { OrbitControls } from '@react-three/drei';
// import { Chair } from './Chair';

export const Experience = () => {
  return (
    <>
      <OrbitControls />

      <Chair scale={1} />
    </>
  );
};
