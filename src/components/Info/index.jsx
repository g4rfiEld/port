import React from 'react';

import github from '../../assets/icons/github.svg';
import mail from '../../assets/icons/mail.svg';
import telegram from '../../assets/icons/telegram.svg';

const pictures = [github, mail, telegram];

export const Info = () => {
  return (
    <>
      <div className="fixed right-5 top-[50%] -translate-y-[50%] max-[600px]:flex max-[600px]:top-[95%] max-[600px]:right-[50%] max-[600px]:translate-x-[50%] max-[600px]:gap-5">
        <div className="w-12 h-12 border-2 border-white flex items-center justify-center rounded-full p-2 mb-5  max-sm:w-10 max-sm:h-10">
          <img src={github} alt="" className="w-auto h-auto" />
        </div>
        <div className="w-12 h-12 border-2 border-white flex items-center justify-center rounded-full p-2 mb-5 max-sm:w-10 max-sm:h-10">
          <img src={mail} alt="" className="w-auto h-auto" />
        </div>
        <div className="w-12 h-12 border-2 border-white flex items-center justify-center rounded-full p-2 max-sm:w-10 max-sm:h-10">
          <img src={telegram} alt="" className="w-auto h-auto" />
        </div>
      </div>
      {/* <div className="fixed left-10 bottom-10 flex items-end gap-1">
        <div className="w-1 h-2 bg-white"></div>
        <div className="w-1 h-1 bg-white"></div>
        <div className="w-1 h-4 bg-white"></div>
        <div className="w-1 h-3 bg-white"></div>
      </div> */}
    </>
  );
};
