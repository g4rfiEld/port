import gsap from 'gsap';
import React, { useLayoutEffect } from 'react';

export const Cursor = () => {
  const cursorRef = React.useRef();
  const followerRef = React.useRef();

  const moveCursor = (e) => {
    gsap.to(cursorRef.current, {
      x: e.clientX,
      y: e.clientY,
    });
    gsap.to(followerRef.current, {
      x: e.clientX,
      y: e.clientY,
      duration: 0.9,
    });
  };

  useLayoutEffect(() => {
    gsap.set(cursorRef.current, {
      xPercent: -50,
      yPercent: -50,
    });
    gsap.set(followerRef, {
      xPercent: 100,
      yPercent: 100,
    });

    window.addEventListener('mousemove', moveCursor);
  }, []);
  return (
    <div className="">
      <div
        ref={cursorRef}
        className="fixed left-20 top-20 translate-x-[-50%] translate-y-[-50%] rounded-full z-50 pointer-events-none   w-2 h-2 bg-white"></div>
      <div
        ref={followerRef}
        className="fixed left-20 top-20 translate-x-[-50%] translate-y-[-50%] rounded-full z-50 pointer-events-none w-14 h-14 border-2 border-white"></div>
    </div>
  );
};
