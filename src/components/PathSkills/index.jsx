import gsap from 'gsap';
import React from 'react';
import { useLayoutEffect } from 'react';
import { Link } from 'react-router-dom';

export const PathSkills = () => {
  const tl = React.useRef();
  const skillsCircleRef = React.useRef();

  const outline1 = React.useRef();
  const outline2 = React.useRef();

  useLayoutEffect(() => {
    let ctx = gsap.context(() => {
      tl.current = gsap
        .timeline({ defaults: { opacity: 1, visibility: 'visible', duration: 0.7 } })
        .to(outline1.current, {})
        .to(outline2.current, {}, '<')
        .fromTo(
          '.path',
          { opacity: 0 },
          { opacity: 1, duration: 0.5, stagger: { from: 'random', each: 0.2 } },
        );
    }, [skillsCircleRef]);

    return () => ctx.revert();
  }, []);

  return (
    <div className="relative  ">
      <div className="container">
        <div className="relative flex items-center justify-center w-full h-[85vh] max-[600px]:h-[80vh]">
          <div
            ref={skillsCircleRef}
            className=" relative w-[25rem] h-[25rem]  flex flex-col items-center justify-center max-md:w-[20rem] max-md:h-[20rem] max-[600px]:w-[12rem] max-[600px]:h-[12rem]">
            <p className="absolute text-[18px] max-[600px]:text-[16px]">Select skill</p>

            <Link
              to={'/skill/frontend'}
              className="path absolute -left-[15%] top-[20px] flex flex-col items-end cursor-pointer z-[2] group max-[600px]:-left-[20%] max-[600px]:-top-[45%]">
              <span className="pr-6 pb-2 max-md:pr-3 max-[600px]:text-[16px]">Frontend</span>
              <div
                className={`relative w-6 h-6  rounded-full border-[2px] border-white before:absolute before:opacity-0  before:duration-500 group-hover:before:opacity-[1] before:left-[50%] before:top-[50%] before:w-2 before:h-2 before:bg-[#fff] before:rounded-full before:-translate-x-[50%] before:-translate-y-[50%]`}></div>
            </Link>
            <Link
              to={'/skill/backend'}
              className="path absolute -right-[22%] top-[20%] flex flex-col items-start cursor-pointer z-[2] group max-[600px]:-right-[20%] max-[600px]:-top-[45%] ">
              <span className="pl-6 pb-2 max-md:pl-3 max-[600px]:text-[16px]">Backend</span>
              <div
                className={`relative w-6 h-6  rounded-full border-[2px] border-white before:absolute before:opacity-0  before:duration-500 group-hover:before:opacity-[1] before:left-[50%] before:top-[50%] before:w-2 before:h-2 before:bg-[#fff] before:rounded-full before:-translate-x-[50%] before:-translate-y-[50%]`}></div>
            </Link>
            <Link
              to={'/skill/other'}
              className="path absolute left-[82%] bottom-0 flex flex-col items-start group cursor-pointer z-[2] max-[600px]:left-[50%] max-[600px]:-bottom-[50%]">
              <div
                className={`relative w-6 h-6  rounded-full border-[2px] border-white before:absolute before:opacity-0  before:duration-500 group-hover:before:opacity-[1] before:left-[50%] before:top-[50%] before:w-2 before:h-2 before:bg-[#fff] before:rounded-full before:-translate-x-[50%] before:-translate-y-[50%]`}></div>
              <span className=" whitespace-nowrap pt-2 pl-6 max-md:pl-3 max-[600px]:text-[16px]">
                Other skills
              </span>
            </Link>

            <div
              ref={outline1}
              className="absolute border-white outline1 w-[22rem] h-[22rem] border-2 max-md:w-[18rem] max-md:h-[18rem] max-[600px]:w-[15rem] max-[600px]:h-[15rem] opacity-0 invisible"></div>
            <div
              ref={outline2}
              className="absolute border-white outline2 w-[22rem] h-[22rem] border-2 max-md:w-[18rem] max-md:h-[18rem] max-[600px]:w-[15rem] max-[600px]:h-[15rem] opacity-0 invisible"></div>
            {/* <div className="absolute ">
              <img src={ring} alt="" className="animate-spin-slow" />
            </div>
            <div className="absolute">
              <img src={ring} alt="" className="animate-spin-very-slow" />
            </div> */}
          </div>
        </div>
      </div>
    </div>
  );
};
