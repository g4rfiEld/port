import React, { useLayoutEffect } from 'react';

import cube from '../../assets/icons_skill/cube.svg';
import gsap from 'gsap';
import { Link } from 'react-router-dom';

const menuArr = ['HOME', 'PROJECTS', 'ABOUT'];

export const Header = () => {
  const [refs, setRef] = useArrayRef();

  const [menu, setMenu] = React.useState(false);

  const tl = React.useRef();

  const spanTopRef = React.useRef();
  const spanMiddleRef = React.useRef();
  const spanBottomRef = React.useRef();

  const menuRef = React.useRef();

  function useArrayRef() {
    const refs = React.useRef([]);
    refs.current = [];
    return [refs, (ref) => ref && refs.current.push(ref)];
  }

  useLayoutEffect(() => {
    let ctx = gsap.context(() => {
      tl.current = gsap
        .timeline({ paused: true })
        .to(spanTopRef.current, { y: 6, duration: 0.2, width: 15 })
        .to(spanBottomRef.current, { y: -6, duration: 0.2, width: 15 }, '<')
        .to(spanMiddleRef.current, { width: 25, height: 2.5, duration: 0.2 })
        .to(menuRef.current, {
          opacity: 1,
          visibility: 'visible',
          duration: 0.5,
        })
        .fromTo(
          refs.current,
          { opacity: 0, visibility: 'hidden', y: 20 },
          {
            opacity: 1,
            visibility: 'visible',
            y: 0,
            stagger: 0.1,
            duration: 0.4,
          },
        );
    });

    return () => ctx.revert();
  }, []);

  React.useEffect(() => {
    menu ? tl.current.play() : tl.current.reverse();
  }, [menu]);

  return (
    <header className="  ">
      <div className="container">
        <div className="relative flex items-center justify-between h-[80px]">
          <Link to={'/'} className="relative w-14 h-14 max-[600px]:w-12 max-[600px]:h-12 z-20">
            <img src={cube} alt="" />
          </Link>
          <div className="relative flex items-center z-20">
            <div className="flex items-center justify-center cursor-pointer w-7 h-7 border-2 border-gray-500 rounded-full mr-10 hover:border-gray-100 group">
              <div className="w-3 h-3 bg-gray-500 rounded-full group-hover:bg-gray-100"></div>
            </div>
            <div
              className="flex justify-center relative h-3.5 w-5 cursor-pointer "
              onClick={() => setMenu(!menu)}>
              <span
                ref={spanTopRef}
                className="absolute top-0 inline-block w-6 h-[1.5px] bg-white"></span>
              <span
                ref={spanMiddleRef}
                className="absolute top-[50%] -translate-y-[50%] inline-block w-4 h-[1.5px] bg-white"></span>
              <span
                ref={spanBottomRef}
                className="absolute bottom-0 inline-block w-6 h-[1.5px] bg-white"></span>
            </div>
          </div>

          <div
            className="fixed left-0 top-0 w-full  bg-black h-screen z-10 text-white text-center opacity-0 invisible vis"
            ref={menuRef}>
            <ul className="relative top-[50%] -translate-y-[50%] font-bold text-[100px] mt-4 max-md:text-[60px] max-[426px]:text-[40px]">
              {menuArr.map((item) => (
                <li className="opacity-0 invisible " ref={setRef}>
                  {item}
                </li>
              ))}
            </ul>
          </div>

          {/* <div className=" w-10 h-10">
          <img src={cube} alt="" />
        </div> */}
        </div>
      </div>
    </header>
  );
};
