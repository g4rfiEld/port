import React, { useLayoutEffect } from 'react';

import s from '../Home.module.scss';
import gsap from 'gsap';

export const HomeItem = ({ index }) => {
  const itemRef = React.useRef();

  const tl = React.useRef();

  useLayoutEffect(() => {
    let ctx = gsap.context(() => {
      tl.current = gsap
        .timeline()
        .fromTo(
          itemRef.current,
          { x: 200, opacity: 0 },
          { x: 0, opacity: 1, duration: 0.5 },
          index * 0.09,
        );
    }, itemRef);

    return () => ctx.revert();
  }, []);

  function generateRandomColor() {
    // Генерируем три случайных значения для красного, зеленого и синего каналов.
    const red = Math.floor(Math.random() * 256);
    const green = Math.floor(Math.random() * 256);
    const blue = Math.floor(Math.random() * 256);

    // Собираем цвет в формате RGB.
    const color = `rgb(${red}, ${green}, ${blue})`;

    return color;
  }

  function getRandomLength() {
    // Генерируем случайную длину (например, от 50 до 200)
    return Math.floor(Math.random() * (200 - 100 + 1)) + 100;
  }

  const gradientStyle = {
    background: `linear-gradient(90deg, ${generateRandomColor()} 0%, ${generateRandomColor()} 100%)`,
    width: getRandomLength(),
  };
  return <div className={s.home__item} style={gradientStyle} ref={itemRef}></div>;
};
