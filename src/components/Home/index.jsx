import React, { useLayoutEffect } from 'react';

import s from './Home.module.scss';
import { HomeItem } from './HomeItem';
import gsap from 'gsap';

// const colorShades = [
//   {
//     length: getRandomLength(),
//     color: ['#FF5733', '#FFC433'],
//   },
//   {
//     length: getRandomLength(),
//     color: ['#33FF57', '#334CFF'],
//   },
//   {
//     length: getRandomLength(),
//     color: ['#FFC433', '#33FFFF'],
//   },
//   {
//     length: getRandomLength(),
//     color: ['#FF33E9', '#33FFFF'],
//   },
// ];

export const Home = () => {
  let tl = React.useRef();

  useLayoutEffect(() => {
    let ctx = gsap.context(() => {
      tl.current = gsap
        .timeline({ defaults: { duration: 1 } })
        .from(
          '.mac-item',

          {
            y: -20,

            opacity: 0,
            ease: 'power2.out',
            stagger: { from: 'start', each: 0.3 },
            yoyo: true,
          },
        )
        .from(
          '.code__up',
          {
            x: 300,
            opacity: 0,
            stagger: { each: 0.04, from: 'start' },
            ease: 'power4.inOut',
          },
          0.9,
        )
        .from(
          '.code__down',
          {
            x: 300,
            opacity: 0,
            stagger: { each: 0.06, from: 'start' },
            ease: 'power4.inOut',
          },
          0.9,
        )
        .from('.home__text', { opacity: 0 });
    });

    return () => ctx.revert();
  }, []);

  return (
    <section className=" w-full min-h-full bg-gray-950 text-white py-14 pl-8  max-md:py-8 max-md:pl-4 max-sm:pl-2 ">
      <div className="container">
        <div className="flex gap-2.5 mb-20 max-md:mb-12">
          <div className="w-5 h-5 bg-red-500 rounded-full mac-item max-md:w-4 max-md:h-4"></div>
          <div className="w-5 h-5 bg-yellow-500 rounded-full mac-item max-md:w-4 max-md:h-4"></div>
          <div className="w-5 h-5 bg-green-500 rounded-full mac-item max-md:w-4 max-md:h-4"></div>
        </div>
        <div className="flex flex-col">
          <div className="w-1/2 max-lg:w-full max-md:w-[80%]">
            <div className="flex flex-wrap gap-2.5 mb-4 max-sm:gap-1.5 max-sm:mb-3">
              <div className="w-28 h-4 bg-gradient-to-r from-yellow-200 to-red-400 code__up max-md:w-20 max-md:h-3 max-sm:w-12 "></div>

              <div className="w-24 h-4 bg-gradient-to-r bg-gray-500 rounded-full code__up max-md:w-16 max-md:h-3 max-sm:w-10 "></div>
              <div className="w-4 h-4 bg-gradient-to-r bg-gray-500 rounded-full code__up max-md:w-3 max-md:h-3 "></div>
              <div className="w-28 h-4 bg-gradient-to-r from-yellow-200 to-red-400 code__up max-lg:w-12 max-md:w-24 max-md:h-3 max-sm:w-16 "></div>
            </div>
            <div className="flex flex-wrap gap-2.5 mb-4 max-sm:gap-1.5 max-sm:mb-3">
              <div className="w-48 h-4 bg-gradient-to-r from-green-300 to-blue-400 code__up max-md:w-30 max-md:h-3 max-sm:w-28 "></div>
              <div className="w-4 h-4 bg-gradient-to-r bg-gray-500  code__up max-md:w-3 max-md:h-3 "></div>
            </div>
            <div className="flex flex-wrap gap-2.5 max-sm:gap-1.5 max-sm:mb-3">
              <div className="w-36 h-4 bg-gradient-to-r from-yellow-200 to-red-400 code__up max-md:w-20 max-md:h-3 max-sm:w-[5rem] "></div>
              <div className="w-40 h-4 bg-gradient-to-r bg-gray-500 rounded-full code__up max-md:w-24 max-md:h-3 max-sm:w-16 "></div>
              <div className="w-4 h-4 bg-gradient-to-r bg-gray-500 rounded-full code__up max-md:w-3 max-md:h-3"></div>
              <div className="w-48 h-4 bg-gradient-to-r from-purple-500 to-pink-500 code__up max-md:w-30 max-md:h-3 max-sm:w-16 "></div>
            </div>
          </div>
          <div className="text py-8 home__text max-md:py-5">
            <span className="text-2xl max-sm:text-xl">&lt;div&gt;</span>
            <div className="p-8 max-md:p-4 ">
              <h1 className="text-6xl mb-4 font-medium max-lg:text-4xl max-sm:text-2xl max-sm:mb-2">
                <span className="text-red-500">&lt;</span>Evgeniy Kobzev /
                <span className="text-blue-600">&gt;</span>
              </h1>
              <p className="text-3xl max-sm:text-xl">Web Developer</p>
            </div>
            <span className="text-2xl max-sm:text-xl">&lt;/div&gt;</span>
          </div>
          <div className="w-1/2 max-lg:w-full max-md:w-[80%] ">
            <div className="flex flex-wrap gap-2.5 mb-11 max-sm:mb-9 max-sm:gap-1.5 ">
              <div className="w-44 h-4 bg-gradient-to-r from-yellow-200 to-red-300 code__down max-md:w-28 max-md:h-3 max-sm:w-20 "></div>
              <div className="w-20 h-4 bg-gradient-to-r from-green-200 to-blue-400 code__down max-md:w-12 max-md:h-3 max-sm:w-8"></div>
              <div className="w-32 h-4 bg-gradient-to-r bg-gray-500 rounded-full code__down max-md:w-24 max-md:h-3 max-sm:w-12"></div>
              <div className="w-4 h-4 bg-gradient-to-r bg-gray-500 rounded-full code__down max-md:w-3 max-md:h-3"></div>
            </div>
            <div className="flex flex-wrap gap-2.5 mb-4 max-sm:gap-1.5 max-sm:mb-3">
              <div className="w-36 h-4 bg-gradient-to-r from-purple-500 to-pink-500 code__down max-md:w-26 max-md:h-3 max-sm:w-[4rem]"></div>
              <div className="w-4 h-4 bg-gradient-to-r bg-gray-500 rounded-full code__down max-md:w-3 max-md:h-3 "></div>
              <div className="w-24 h-4 bg-gradient-to-r bg-gray-500 rounded-full code__down max-md:w-16 max-md:h-3 max-sm:w-20"></div>
            </div>
            <div className="flex flex-wrap gap-2.5 max-sm:gap-1.5 max-sm:mb-3">
              <div className="w-48 h-4 bg-gradient-to-r from-green-300 to-blue-400 code__down max-md:w-30 max-md:h-3 max-sm:w-24"></div>
              <div className="w-4 h-4 bg-gradient-to-r bg-gray-500 code__down max-md:w-3 max-md:h-3"></div>
              <div className="w-32 h-4 bg-gradient-to-r from-yellow-200 to-red-400 code__down max-md:w-20 max-md:h-3 max-sm:w-16"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
{
  /* <div className="container">
        <div className={s.home__row}>
        
          <div className={s.home__row__content}>
            <div className={s.home__row__code}>
              {[...new Array(10)].map((_, index) => (
                <HomeItem index={index} />
              ))}
            </div>
            <div className={s.home__row__info}>
              <pre></pre>
              <h1 className={s.home__row__name}>Evgeniy Kobzev</h1>
              <p className={s.home__row__subname}>Web Developer</p>
            </div>
            <div className={s.home__row__code}>
              {[...new Array(10)].map((_, index) => (
                <HomeItem index={index} />
              ))}
            </div>
          </div>
        </div>
      </div> */
}
