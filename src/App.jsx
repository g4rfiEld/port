import { Routes, Route } from 'react-router-dom';

import { Experience } from './components/Experience';
import { Home } from './components/Home';
import { Info } from './components/Info';
import { Cursor } from './components/Cursor';
import { Skills } from './components/Skills';
import { PathSkills } from './components/PathSkills';
import { SkillCircle } from './components/SkillCircle';
import { Header } from './components/Header';

function App() {
  return (
    <div className="relative  bg-gray-950 min-h-screen text-white flex flex-col">
      <Header />
      <Routes>
        <Route path="/" element={<PathSkills />} />
        {/* <Route path="/about" element={<About />} /> */}
        <Route path="/skill/:name" element={<SkillCircle />} />
        {/* <Route path="/projects" element={<Projects />} /> */}
      </Routes>
      {/* <Home /> */}

      {/* <SkillCircle /> */}

      <Info />

      {/* <Cursor /> */}
    </div>
  );
}

export default App;
